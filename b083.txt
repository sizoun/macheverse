



























						8
					      88
					    888
					    88
					    88
					      8	
					       8
						8  
	-Quand j’était jeune ado, 13/14 ans,     88
	je me repérais super mal dans Paris,      88
	voir pas du tout.    			   88
	Je voyais le plan de tête  		   888
	mais il n’y avait pas transpositions.	    888
	 	Et je n’avais pas de 4G. 	     88
	J’étais au Jardin des Plantes,		     88
	et je voulais rentrer comme d’habitude       88
	en prenant le bus 24.			     8
	Sauf que là, la je n’avais plus d’argent    88
	et mon ticket s’était plié dans ma poche    8
	le chauffeur de bus m’a engueulé.	   88
	Ça m’a tellement énervé,                  88
	je suis descendu du bus			 88
	et je me suis promis 		        88
	de ne plus jamais en prendre un de ma vie.
					      88
	Il était midi, je devait être rentré à 13h, 
	J’ai commencé à suivre la Seine.      88
	Je me repérais avec les deux cheminées. 8
	Je me suis retrouvée bloqué devant l’échangeur,
	avec devant moi, les deux cheminées.     88
	J’ai fini par prendre un bus		  88
	et rentrer chez moi. 			   8
						     8
						      8
						     8
						    8     -C’est là que tu as commencé
						   8      à vraiment t’intéresser à l’usine ?
						 88
					      8888
	-Non, c’est en m’intéressant         8888
	à l’urbanisme et a l’architecture     88888
	de la ville d’Ivry. 			88888
	Notamment les opérations		  88888
	qui ont étés menées par Jean Renaudie,     8888888
	Renée Gallhouistet, et Nina Schuch,           88888
	Dans les années 70. 				8888
	A la base, Jean Renaudie était marié avec        8888888
	Renée Gallhouistet, mais ils ont fini par se séparer88888
	et il est sorti avec Nina Schuch.                    8888
							    888      -Le salaud !
	-Oui. C’est des bâtiments vraiment corbusiéens,    88
	C’est relativement éloigné de l’usine,	           8
	mais proche d’une autre usine,			  8
	qui forme la troisième cheminée d’Ivry.          88
	Elle est aussi grande que les deux autres,     888
	mais celle là ne fume pas.    		       88
						        888    -Donc on s'en fiche de celle là.
	-c’est quand même un monument important d’Ivry.   88
							  8  -Oui mais c’est moins frappant
							 8    vu qu’elle ne fume pas.
	-Oui mais elle est blanche et bleue, et belle  88
						       88   -D’accord.
	-Et il y a de la vapeur qui sort,	    8888
	mais devant l’usine, 			    88
	c’est assez impressionnant.                88
	Je décris un détail,			  8
	mais c’est une bouche d’aération,       88
	sur le trottoir devant cette usine.    88
	C’est très post-soviétique,             8	
	en hiver c’est frappant.                88
	C’est à côté de la passerelle qui relie Ivry et Charenton,
					  	8 -Tu es jaloux que j’ai pu rentrer dans l’usine
						  88  et pas toi ?
	-Oui, très.                              88
						-Tu aimerais y aller avant son démantèlement ?
	-Non c’est bon j’ai vu tes photos,    888
	c’est bien assez.		     88
	Si ils faisaient des vues 360        8
	une aprem, et voila, ta une bête de truc.
	Faudrait que quelqu’un lui dise !   8
					   8  -Oui mais toi tu en penses quoi de l’usine ?
	-Je pense que je préférerais      8
		sans			 88
		les			88
		cheminées.	       88                 
	Les cheminées ça fait un peu too much,                
	je pense que je préférais sans. 88              